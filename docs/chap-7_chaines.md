Les chaînes de caractères (ou *strings* en anglais) sont un moyen de stocker du texte dans une variable.
On en a déjà manipulé depuis le début de ce cours mais ici, on va s'y pencher plus en détail.

On rappelle pour que déclarer une chaîne, il suffit de taper le texte entre des guillemets.

```python
prenom = "Michel"

#On peut aussi utiliser des apostrophes ''
nom = 'Ocelot'
```

Si jamais vous voulez utiliser des guillemets dans une chaîne, vous pouvez soit:

- Utiliser des apostrophes
  `histoire = 'Je cite: "L'âne est très beau."'`

- Utiliser des guillemets et échapper les *sous-guillements* avec un anti-slash `\`
  `histoire = "Je cite: \"L'âne est très beau.\"" `

  Quand on échappe un caractère, Python le considère comme du texte au lieu du rôle spécial qui a.
  Dans le cas des guillements, échapper un guillement permet de dire à Python de traîter le guillement comme un caractère guillement et non pas comme la fin de la déclaration de la chaîne.

  Exemples de caractères spéciaux:

  * Retour à la ligne `\n`

    ```python
    histoire = "L'âne est très beau.\nLe ciel est bleu."
    print(histoire)
    """
    Affiche
    L'âne est très beau.
    Le ciel est bleu.
    """
    ```

  * Tabulation `\t`

    ```python
    ligne = "Nom\tPrénom\t\tAdresse"
    print(ligne)
    # Affiche Nom	Prénom		Adresse
    ```

Contrairement à d'autres langages de programmation, les strings en Python sont **mutables**, ce qui veut dire que l'on peut modifier leur valeur après les avoir déclarées.

```python
nom = "Osa"
nom = "Flores"
```

On peut concaténer deux strings avec l'opérateur `+`

```python
prenom = "Zinedine"
nom = "Zidane"
nom_complet = prenom + " " + nom
print(nom_complet) # Zinedine Zidane
```

On peut ajouter du texte à une chaîne avec l'opérateur `+=`

```python
histoire = "Il était une fois"
histoire += " un petit prince"

print(histoire) # Il était une fois un petit prince
```

### Travailler avec des chaînes

Les chaînes se gèrent comme des listes de caractères, ce qui veut dire que toutes les méthodes applicables aux listes s'appliquent également aux *chaine*.

Par exemple, comme avec les listes, on peut vérifier qu'une chaîne contient une autre chaîne avec *in*

```python
def contient(chaîne, recherche):
    if (recherche in chaîne):
        print("{recherche} est dans {chaîne}")
        
  	else:
        print(f"{recherche} n'est pas dans {chaîne})
              
 	return
```

PS: Par convention dans les langages de programmation, on utilise l'analogie de l'aiguille et la botte de foin quand on parle de rechercher quelque chose.
Par exemple, si je veux vérifier si `Joyeux anniversaire !` contient un `a`, `Joyeux anniversaire` ! est ma botte de foin (*haystack* en anglais) et `a` est l'aiguille (*needle* en anglais).
On retrouve souvent cette teminologie dans les documentations ou certains tutoriels donc il me paraît important de le préciser :)

Pour extraire un morceau d'une chaîne (on appelle ça une *sous-chaîne*), on utilise la même notation que pour extaire une sous-listed'une liste.

```python
chaine = "Joyeux anniversaire !"

# Du 3ème au 5ème caractère -> yeu
sous_chaine = chaine[2:5]
print(sous_chaine)
```

Les chaînes ont aussi des méthodes qui leur sont propres.

#### Convertir un nombre en une chaîne

Avec la méthode `str()`

```python
annee = 2021
annee_str = str(annee) # C'est comme si on avait déclaré annee_str = "2021"
```

#### Convertir une chaîne représentant un nombre en un nombre

Si c'est un nombre entier, on utilise `int()`
Si c'est un nombre décimal, on utilise `float()`

```python
annee_str = "2021"
annee = int(annee_str) # annee vaut 2021 (le nombre)

pi_str = "3.14"
pi = float(pi_str) # pi vaut 3.14 (le nombre)
```

Vous n'utiliserez probablement pas ça quand vous commencerez mais il est possible de spécifier la base dans laquelle le *nombre-chaîne* est écrit.

```python
dix_base_2 = "1010"
dix_base_10 = int(dix_base_2, 2) # dix base 10 vaut 10
```

Voici un <a href="https://lehollandaisvolant.net/tuto/bin/" target="_blank" rel="noreferrer">cours sur le système binaire</a> si jamais vous voulez comprendre comment fonctionne la notation en base 2 (toutes les autres bases fonctionnent approximativement sur le même système)

#### Transformer une chaîne en majuscule/minuscule

```python
chaine = "Hello World !"

string_majuscule = chaine.upper() # HELLO WORLD !
string_minuscule = chaine.lower() # hello world !
```

#### Vérifier si une chaîne a certaines caractéristiques

##### Entièrement en majuscule/minuscule

On peut vérifier si l'entièreté d'une chaîne est en majuscule ou en minuscule - on appelle cette propriété la *casse* - avec respectivement `isupper()` et `islower()`

```python
def verifie_casse(chaine):
    if (chaine.isupper()):
        print(f"{chaine} est en majuscule.")
        
   	elif (chaine.islower()):
        print(f"{chaine} est en minuscule.")
        
   	else:
        print(f"{chaine} n'est ni en majuscule, ni en minuscule.")
        
 	return
```

##### Ne contient que des chiffres et des lettres

On peut vérifier si une chaîne ne contient que des chiffres ou des lettres (= est *alphanumérique*) avec la méthode `isalpha()`

```python
def verifie_alpha_num(chaine):
    if (chaine.isalpha()):
        print(f"{chaine} est une chaîne alphanumérique.")
        
   	else:
        print(f"{chaine} n'est pas alphanumérique.)
              
   	return
```

##### Commence/finit par une autre chaîne

On peut vérifier si une chaîne commence/finit par une autre chaîne respectivement grâce aux méthodes `startswith()` et `endswith()`

```python
def verifie_debut_fin(chaine, morceau):
    if (chaine.startswith(morceau)):
        print(f"{chaine} commence par {morceau}")
        
  	elif (chaine.endswith(morceau)):
        print(f"{chaine} finit par {morceau}")
        
  	else:
        print(f"{chaine} ne commence et ne finit pas par {morceau}")
        
  	return
```

### Bonus: améliorer *FizzBuzz*

Maintenant qu'on a appris à manipuler les chaînes de caractère, on peut apprendre à coder une autre des nombreuses façons de résoudre *FizzBuzz* (si vous avez oublié ce que c'est, vous pouvez aller au [chapitre 5](https://alsagone.gitlab.io/cours-python/chap-5_programmes-avances/#fizzbuzz) pour vous rafraîchir la mémoire.)

```python
"""
Prend en paramètre un nombre n et renvoie:
	"Fizz" si le nombre est un multiple de 3 (et pas un multiple de 5)
	"Buzz" si le nombre est un multiple de 5 (et pas un multiple de 3)
	"FizzBuzz" si le nombre est à la fois un multiple de 3 et de 5
	le nombre sous forme de chaîne s'il n'est pas un multiple de 3 ni de 5
"""
def get_fizz_buzz(n):
    # On part avec un résultat vide
    resultat = ""
    
    # On rajoute Fizz au résultat si le nombre est un multiple de 3
    if (n % 3 == 0):
        resultat += "Fizz"
    
    # On rajoute Buzz au résultat si le nombre est un multiple de 5
    # Du coup, si le nombre à la fois un multiple de 3 et de 5, on a FizzBuzz
    if (n % 5 == 0):
        resultat += "Buzz"
    
    # Si le resultat est vide à ce stade du code, ça veut dire que le nombre n'est ni un multiple de 3,
	# ni un multiple de 5 donc le résultat est simplement le nombre converti en string
    if (len(resultat) == 0):
        resultat = str(n)
        
    return resultat

def fizz_buzz_v2(limite):
    debut = 1
    fin = limite + 1
    for i in range(debut, fin):
        print(get_fizz_buzz(i), end=" ")	#On sépare le tout par des espaces
        
    print() # Retour à la ligne une fois fini
    return

if __name__ == "__main__":
    fizz_buzz_v2(20)
	# Affiche 1 2 Fizz 4 Buzz Fizz 7 8 Fizz Buzz 11 Fizz 13 14 FizzBuzz 16 17 Fizz 19 Buzz
```

 
