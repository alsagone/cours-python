Ce que vous allez apprendre dans ce chapitre

- Déclarer une variable
- Les différents types de variables
- Afficher la valeur d'une variable

Qu'est-ce qu'une variable ? Une variable, il faut voir ça comme une boîte où on stocke une information qu'on veut réutiliser plus tard.

Il existe plusieurs types de variables:

- les entiers (1, 2, 3, etc...)
- les flottants (0.25, 3.14 etc...)
- les strings/chaînes de caractères ("Bonjour", "Hello World !")
- les booléens (True/False pour vrai/faux)

Contrairement à la majorité des langages de programmation, pour déclarer une variable, il n'y a pas besoin de spécifier son type.
Il suffit juste de donner le nom de la variable, le signe égal et la valeur que vous voulez lui assigner.

```python
if __name__ == "__main__":
  nom = "Bob" # Chaîne de caractères
  age = 26 # Entier
  salaire = 1034.73 # Flottant
  travaille = True # Booléen

  print(nom)
  print(age)
  print(salaire)
  print(travaille)

"""
Affiche
Bob
26
1034.73
True
"""
```

Pour afficher la valeur d'une variable, on peut utiliser l'instruction print, suivi du nom de la variable en parenthèses.
Mais on peut aussi combiner variables et texte de la façon suivante:

```python
if __name__ == "__main__":
  nom = "Bob"
  age = 26
  salaire = 1034.73
  travaille = True

  print(f"Je m'appelle {nom}, j'ai {age} ans et mon salaire est de {salaire} euros.")

# Affiche "Je m'appelle Bob, j'ai 26 ans et mon salaire est de 1034.73 euros.
```

Notez que la parenthèse du `print` commence par un `f`, c'est pour signifier à Python que l'on compte utiliser des noms de variable dans notre chaîne de caractères.
Les variables sont entourées d'une paire d'accolades pour signifier à Python d'afficher la valeur de la variable correspondante.
Sans accolades, Python aurait interprété nos noms de variables comme du texte et aurait simplement affiché `Je m'appelle nom, j'ai age ans et mon salaire est de salaire euros.`

Les variables servent également à stocker le résultat que renvoie une fonction. Par exemple, si on reprend notre calcul d'aire de rectangle du chapitre précédent, on peut l'améliorer de telle sorte à ce qu'il affiche la valeur de l'aire.

```python
# Prend en paramètre la largeur (l) et la hauteur (h) d'un rectangle et renvoie son aire
def aire_rectangle(l, h):
  return l * h

if __name__ == "__main__":
  largeur = 10
  hauteur = 8
  aire = aireRectangle(largeur, hauteur)

  print(f"L'aire du rectangle de largeur {largeur} et de hauteur {hauteur} vaut {aire}.")

# Affiche L'aire du rectangle de largeur 10 et de hauteur 8 vaut 80.
```

Bien évidemment, si on doit afficher l'aire de plusieurs rectangles, il est plus judicieux de faire notre `print` directement dans la fonction `aireRectangle`

```python
# Prend en paramètre la largeur (l) et la hauteur (h) d'un rectangle et affiche son aire
def aire_rectangle(l, h):
  aire = l * h
  print(f"L'aire du rectangle de largeur {l} et de hauteur {h} vaut {aire}.")
  return

if __name__ == "__main__":
  aireRectangle(10, 8)
  aireRectangle(2, 50)
  aireRectangle(5, 3)

"""
Affiche
L'aire du rectangle de largeur 10 et de hauteur 8 vaut 80.
L'aire du rectangle de largeur 2 et de hauteur 50 vaut 100.
L'aire du rectangle de largeur 5 et de hauteur 3 vaut 15.
"""
```

## *None*

*None* est un mot-clé pour dire que le contenu d'une variable est *vide*. C'est utile dans les cas où  par exemple, on veut déclarer une variable au début de notre code et ensuite lui assigner une valeur plus tard.

```python
x = None
# ...
x = 5
```



## Exercice - Afficher une somme

En reprenant la même structure que ci-dessus, écrivez une fonction qui prend en paramètres deux nombres a et b et qui affiche leur somme.

Par exemple:

```
10 + 8 = 18
2 + 50 = 52
5 + 3 = 8
```

Solution

```python
# Prend en paramètre deux nombres (a et b) et affiche leur somme
def somme(a, b):
    s = a + b
    print(f"{a} + {b} = {s}")
    return


if __name__ == "__main__":
    somme(10, 8)
    somme(2, 50)
    somme(5, 3)

"""
Affiche
10 + 8 = 18
2 + 50 = 52
5 + 3 = 8
"""
```

## Exercice - afficheInfos

Écrivez une fonction qui prend en paramètre un nom, un âge et un salaire et les affiche dans une phrase.

```python

def affiche_infos(nom, age, salaire):
  # À compléter

if __name__ == "__main__":
  affiche_infos("Bob", 26, 1034.73)

  # Affiche "Je m'appelle Bob, j'ai 26 ans et j'ai un salaire de 1034.73 euros.
```

Solution

```python

def affiche_infos(nom, age, salaire):
  print(f"Je m'appelle {nom}, j'ai {age} ans et j'ai {salaire} euros.")
  return

if __name__ == "__main__":
  affiche_infos("Bob", 26, 1034.73)
  affiche_infos("Alice", 35, 2048.10)

  """
  Affiche
  Je m'appelle Bob, j'ai 26 ans et j'ai un salaire de 1034.73 euros.
  Je m'appelle Alice, j'ai 35 ans et j'ai un salaire de 2048.10 euros.
  """
```
