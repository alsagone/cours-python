Les classes permettent de créer des objets réutilisables.

Disons que vous êtes un‧e professeur‧e et que vous voulez faire une liste de vos élèves.
La première approche serait de faire une liste `[prenom, nom, age]` et de faire une grande liste de ces petites listes.

```python
def creer_eleve(prenom, nom, age):
    return [prenom, nom, age]

eleve_1 = creer_eleve("Marcus", "Holloway", 25)
eleve_2 = creer_eleve("Aiden", "Pearce", 33)
eleve_3 = creer_eleve("Elise", "De La Serre", 24)
ma_classe = [eleve_1, eleve_2, eleve_3]

```

Ce code n'a rien de mauvais en soi, mais on se heurte rapidement à un obstacle: notre code n'est pas facilement modifiable.
Si vous devez rajouter une donnée dans la description des élèves - par exemple, le nom de leur animal de compagnie, vous devez modifier la fonction `creer_eleve()` et rajouter l'information à chaque élève - sans compter le fait que certains élèves n'ont pas d'animal de compagnie.

On n'a que 6 lignes de codes donc on _pourrait_ se contenter de modifier le tout à chaque fois que notre supérieur‧e nous demande de rajouter une information mais sur un projet de plusieurs centaines de lignes, c'est rapidement ingérable.

Pour palier à ça, il existe le concept de **classe**. Un élève se décrit par la classe suivante:

```python
class Eleve:
    def __init__(self, prenom, nom, age, nom_animal);
    	self.prenom = prenom
        self.nom = nom
        self.nom_animal = nom_animal

eleve_1 = Eleve("Marcus", "Holloway", 25, "Wrench Junior")
eleve_2 = Eleve("Aiden", "Pearce", 33, "Desmond")
eleve_3 = Eleve(("Elise", "De La Serre", 24, "Arno")

```

Décortiquons tout ça:

Pour définir une classe, on utilise le mot-clé `class` suivi du nom de la classe.
Ensuite vient la fonction `__init__`: derrière ce nom barbare se cache tout simplement la fonction d'initialisation où on dit à Python ce que l'on veut dans notre classe.
Ici, on veut stocker le nom, le prénom et le nom de l'animal de compagnie donc on les passe comme arguments:

```python
def __init__(self, prenom, nom, age, nom_animal);
	# À compléter
```

Le mot-clé `self` (_lui-même_ en anglais) fait référence l'objet crée par la classe. Plus précisément, le premier argument d'une fonction de classe fait référence à l'objet - il n'est pas obligatoire d'utiliser `self`, vous pouvez le nommer comme vous voulez mais par convention, on utilise `self`.

On a trois variables `prenom`, `nom` et `nom_animal` mais notre objet Eleve ne les utilise pas encore: on crée alors trois champs du même nom en utilisant le mot-clé `self`

```python
def __init__(self, prenom, nom, age, nom_animal):
    self.prenom = prenom
    self.nom = nom
    self.age = age
    self.nom_animal = nom_animal
```

Maintenant, on peut accéder aux trois champs de notre objet grâce à la syntaxe `objet.nomChamp` 
Par exemple:

```python
class Eleve:
    # ...

marcus = Eleve("Marcus", "Holloway", 25, "Wrench Junior")
print(marcus.nom) # Affiche Marcus
```

Disons maintenant qu'en plus de créer un objet `Eleve`, vous voulez avoir une fonction qui affiche ses informations.
On peut créer une fonction à l'intérieur de notre classe et l'appeler par la syntaxe `objet.fonction()`

```python
class Eleve:
    def __init__(self, prenom, nom, nom_animal):
    self.prenom = prenom
    self.nom = nom
    self.nom_animal = nom_animal

    def affiche_infos(self):
        print(f"{self.nom} {self.prenom} - {self.nom_animal}")
        
eleve = Eleve("Marcus", "Holloway", "Wrench Junior")
eleve.affiche_infos()
# Affiche Marcus Holloway - Wrench Junior
```
