Ce que vous allez apprendre dans ce chapitre:

- Les différents types d'opérateurs
- Les conditions
- Les boucles

![Owen Wilson qui dit Wow](images/wow.jpg)

## Les opérateurs

### Les opérateurs classiques

- Addition `+`
- Soustraction `-`
- Multiplication `*`
- Division `/`

À ça, il faut ajouter le _modulo_ `%`: le modulo permet de récupérer le reste de la division euclidienne de deux nombres.
Par exemple: `7 / 2 = 2 * 3 + 1` -> 1 est le reste de la division euclidienne de 7 par 2 donc `7 % 2 = 1`
Le modulo est utile pour connaître si un nombre est un multiple d'un autre.
Par exemple, pour vérifier si un nombre `n` est divisible par 2, on vérifie si `n % 2` vaut 0.

### Les opérateurs de comparaison

Soient deux variables `a` et `b`

- Égalité: `a == b`
- Différence: `a != b`
- Strictement inférieur: `a < b`
- Strictement supérieur: `a > b`
- Inférieur ou égal: `a <= b`
- Supérieur ou égal: `a >= b`

## Les conditions

Avec ces opérateurs, on peut dire à Python de n'exécuter du code que sous certaines conditions. Pour ceci, on utilise l'instruction `ìf... else...` (qui se traduit en français par `si... sinon...`)

Elle fonctionne de la façon suivante:

```python
if (condition):
	# exécuter toutes les instructions de ce bloc
	# si la condition est remplie

else:
	# exécuter toutes les instructions de ce bloc
	# si la condition n'est pas remplie

# Le programme continue ici
```

Si on veut tester plusieurs conditions dans un même bloc `if... else`, on peut utiliser le mot-clé `elif` (qui est la contraction de `else` et `if`).
Par exemple, on peut coder une fonction qui affiche si un nombre est un multiple de 2 ou un multiple de 3..

```python
def est_un_multiple_de_2_ou_3(n):
    if (n % 2 == 0):
        print(f"{n} est un multiple de 2.")

    elif (n % 3 == 0):
        print(f"{n} est un multiple de 3.")

    else:
        print(f"{n} n'est ni un multiple de 2, ni un multiple de 3.")

    return


if __name__ == "__main__":
    est_un_multiple_de_2_ou_3(10)
    est_un_multiple_de_2_ou_3(33)
    est_un_multiple_de_2_ou_3(37)

"""
    Affiche
    10 est un multiple de 2.
    33 est un multiple de 3.
    37 n'est ni un multiple de 2, ni un multiple de 3.
"""
```

## Les boucles

### La boucle _for_

La boucle `for` permet de répéter plusieurs instructions un certain nombre de fois.
Sa syntaxe est de la forme:

```python
for variable in range(début, fin):
	# Instructions
```

Par convention dans tous les langages de programmation, on choisit `i, j, k etc...` comme noms de variables pour une boucle `for` mais rien en vous empêche d'utiliser votre propre nom de variable.
![For loop](images/forLoop.jpg)

Par exemple, pour afficher tous les nombres de 1 à n.

```python
	def affiche_nombres(n):
		for i in range(1, n+1):
			print(i, end=" ")
			# Le end=" " permet d'afficher un espace
			# après chaque nombre,
			# au lieu de revenir à la ligne

        print("")
        #Pour revenir à la ligne

		return

	if __name__ == "__main__":
		affiche_nombres(10)

	# Affiche 1 2 3 4 5 6 7 8 9 10
```

Vous remarquerez que pour aller de 1 à 10, la fin de la boucle `for` doit être égale à 11.
C'est une habitude à prendre et ça deviendra automatique avec le temps mais pour ne pas me tromper au début, je déclarais deux variables `debut` et `fin` et j'écrivais `for i in range(debut, fin+1)`.

Par exemple:

```python
def afficheNombres(n):
	debut = 1
	fin = n

	for i in range(debut, fin+1):
		print(i, end=" ")

	print("")
	return
```

Par défaut, Python augmente la valeur de la variable que vous utilisez dans la boucle `for` de 1 à chaque fois, mais il est possible de définir sa propre valeur d'incrémentation.
Par exemple, pour augmenter `i` de 2 à chaque passage:

```python
for i in range(debut, fin+1, 2):
```

On peut aussi utiliser une variable d'incrémentation négative pour aller en arrière. Par contre dans ce cas, il faut bien faire attention que la variable de début soit supérieure à la date de fin.
Par exemple, pour afficher tous les nombres de 10 à 1:

```python
def affiche_arriere(n):
    debut = 10
    fin = 1
    for i in range(debut, fin-1, -1):
        print(i, end=" ")

    print("")
    return

if __name__ == "__main__":
    affiche_arriere(10)

    # Affiche 10 9 8 7 6 5 4 3 2 1
```

Remarquez que dans ce cas, la variable de fin vaut `fin - 1` pour la même raison que tout à l'heure.

### La boucle _while_

La boucle `while` permet d'exécuter un bloc d'instructions tant qu'une condition est remplie.

Par exemple, on peut reprendre notre fonction qui affiche tous les nombres de 1 à `n` et remplacer la boucle `for` par une boucle `while`.

```python
def affiche_nombres_v2(n):
	i = 1

	while (i <= n):
		print(i, end=" ")
		i += 1   #Équivalent de i = i + 1

	print(" ")
	return
```

Décortiquons ce qu'il se passe:

- Au début, on initialise notre variable `i` à 1.
- Début de la boucle `while`: exécuter tout le bloc en dessous tant que `i` est inférieur ou égal à `n`
  - Afficher la valeur de `i`
  - Augmenter i de 1
- Sortie de la boucle, la fonction s'arrête.

### Comment choisir quel type de boucle à utiliser ?

Même si les boucles `for` et `while` fonctionnent sensiblement pareil, il y a un certain cas où on préfère utiliser l'une à l'autre.

Reprenons notre fonction qui affiche des nombres de 1 à `n`.

```python
def affiche_nombres_for(n):
	debut = 1
	fin = n

	for i in range(debut, fin+1):
		print(i, end=" ")

	print("")
	return

def affiche_nombres_while(n):
    i = 1
   	while (i <= n):
        print(i, end=" ")
        i += 1

    print("")
    return

if __name__ == "__main__":
    affiche_nombres_for(10)
    affiche_nombres_while(10)

```

Les deux fonctions affichent exactement la même chose et fonctionnent exactement pareil. Par contre, avec la version `while`, on doit:

- déclarer et initialiser `i` à 1
- Tester à chaque boucle si `i` est toujours inférieur ou égal à `n`
- Augmenter `i` de 1

Alors qu'avec la version `for`, un simple `for i in range(1, n+1)` suffit.

En règle générale, quand vous savez que vous devez exécuter un bloc d'instructions un certain nombre de fois et que vous connaissez ce nombre à l'avance, il vaut mieux utiliser une boucle `for`.
Quand vous ne savez pas à l'avance ou que vous n'êtes pas sûr‧e, utilisez une boucle `while` - vous pouvez toujours modifier votre code en cours de route si vous vous rendez compte qu'utiliser une boucle `for` est plus judicieux.

Ce n'est pas nécessairement mauvais d'utiliser une boucle `while` mais disons que c'est une version un peu "lourde".

### À l'aide, mon programme ne s'arrête pas !

Disons qu'avec un peu de fatigue, vous avez écrit une boucle qui ne s'arrête pas. Par exemple:

```python
while (1 == 1):
    print("À L'AIDE")
```

1 est toujours égal à 1 donc la boucle ne va jamais se terminer et le programme va afficher `À L'AIDE` en boucle.
Pour pouvoir arrêter le programme de force, appuyez sur la touche `Ctrl` (en bas à gauche du clavier) et la touche `C` en même temps: le raccourci ++ctrl+c++ envoie un signal à Python pour lui dire de s'arrêter immédiatement (quelque soit ce qu'il est en train de faire) - d'où le `KeyboardInterrupt` (interruption clavier) qu'il marque.

![Arrêter une boucle infinie](images/boucleInfinie.jpg)

Maintenant qu'on a appris à utiliser les boucles et les conditions, on peut créer des programmes un peu plus avancés. Dans le chapitre suivant, on va s'attaquer au jeu du _FizzBuzz_ et le jeu _Plus ou moins ?_.
