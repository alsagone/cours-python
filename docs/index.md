---
title: Introduction au Python
description: Petit cours d'introduction au Python, fait pour être compris même si vous n'avez jamais codé de votre vie
---

Bonjour ! o/

Vu que je me débrouille plutôt pas mal en Python et que mes ami‧e‧s me demandent souvent comment j'ai fait pour apprendre, je me suis dit que j'allais écrire une petite série de cours avec pour objectif qu'ils soient compréhensibles par une personne qui n'a jamais touché un seul langage de programmation.

On va passer en revue les bases du Python et s'en servir pour coder des scripts qui seront de plus en plus élaborés, mais n'ayez pas peur j'irai vraiment doucement et j'expliquerai tout dans les moindres détails.

Si jamais vous avez le moindre problème ou la moindre question, vous pouvez m'envoyer un message sur [Twitter](https://twitter.com/alsagone), j'y réponderai avec plaisir !

PS: si ça vous plaît et si vous voulez me donner un peu de sous, j'ai un [Ko-fi](https://ko-fi.com/alsagone) ! :)

<a href="https://ko-fi.com/alsagone" target="_blank" rel="noreferrer" id="kofi">
<img src="images/kofi_ours_bonjour_gay.png" alt="Kofi Ours Bonjour" />
</a>
