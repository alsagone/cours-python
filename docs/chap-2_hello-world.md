Un _Hello World_ en programmation, c'est souvent le premier programme qu'écrit quelqu'un qui apprend à coder, c'est aussi généralement le programme qu'on écrit pour vérifier si tout s'est installé correctement et fonctionne.

Au final, notre Hello World ressemblera à ça, je sais que ça a l'air impressionnant comme ça mais faire cet Hello World est une bonne introduction à la syntaxe du Python.

![Hello World](images/hello_world.png)

Pour commencer, ouvrez un nouveau fichier avec Visual Studio Code (Fichier -> Nouveau fichier) ou tout simplement avec le raccourci _Ctrl + N_.
Enregistrez ce fichier (je vous conseille de créer un dossier _Scripts_ pour stocker tout ce qu'on va coder pendant ce cours) en l'appelant `helloWorld.py`, ce qui a pour effet d'activer les extensions Python qu'on a installées au chapitre précédent.

Vous pouvez maintenant retaper le code ci-dessus dans le fichier - en prenant en compte le fait que les espaces blancs aux lignes 2, 3 et 7 sont des tabulations (on parlera de l'importance des tabulations plus tard)
Avant de le lancer, je vais décortiquer ce qu'il s'y passe.

```python
def hello_world():
    print("Hello World!")
    return
```

Ceci est une **fonction**, les fonctions sont une suite d'instructions qu'on peut aisément appeler dans notre programme au lieu de ré-écrire ces instructions.

Par exemple, mettons nous dans un cas où on doit calculer l'aire d'un rectangle (largeur \* hauteur): au lieu de ré-écrire à chaque fois la multiplication pour chaque rectangle, vous pouvez écrire la fonction suivante:

```python
def aire_rectangle(largeur, hauteur):
  return largeur * hauteur
```

Et ensuite l'appeler ainsi:

```python
aire_1 = aire_rectangle(5, 10)  # Retourne 50
aire_2 = aire_rectangle(10, 8) # Retourne 80
```

Pour définir une fonction, on utilise le mot-clé `def` suivi du nom de la fonction et les arguments de la fonction entre parenthèses (les éléments que va utiliser la fonction) - si la fonction n'a besoin d'aucun argument, on peut laisser les parenthèses vides.
Après les parenthèses, il faut mettre un `:` puis vous pouvez ensuite écrire vos instructions.

Le coeur de la syntaxe du Python repose sur l'indentation par des tabulations: voyez ça comme un moyen de grouper nos instructions, comme on hiérarchierait du texte.

Pour nous aider à nous repérer, l'extension `indent-rainbow` colorie chaque indentation. Chaque instruction de la fonction doit commencer par une tabulation. Par exemple:

![long programme](images/longProgramme.png)

Le mot-clé `return` siginfie à Python que la fonction s'arrête ici.
On peut également si besoin, retourner un résultat comme pour la fonction `aireRectangle` de tout à l'heure.

```python
def aire_rectangle(largeur, hauteur):
  return largeur * hauteur

aire = aire_rectangle(10, 5) # aire vaut 50
```

On se penchera plus en détail sur les fonctions mais retenez que c'est un moyen de rendre notre code plus clair et plus lisible si on doit répéter une tâche plusieurs fois.

Par convention, les noms de fonction ou de variables (on en parlera au chapitre suivant) doivent respecter la *snake case*: tout en minuscule et les mots séparés par des *underscores*.
Par exemple, `nom_de_fonction_tres_long`

Autre moyen de rendre notre code plus lisible est les commentaires.
C'est important de commenter son code si vous codez à plusieurs pour ne pas que vos collègues ne soient perdu‧e‧s dans votre code (croyez-moi, lire le code de quelqu'un d'autre est une tâche ardue parce que chacun a son propre style de programmation) mais aussi pour vous-même. Je me suis souvent retrouvé dans des cas où je reprenais un projet vieux de plusieurs mois que j'ai codé moi-même sans l'avoir commenté et j'ai dû me prendre quelques minutes pour comprendre ce que j'avais codé.

Il y a deux types de commentaires en Python: le commentaire en une ligne avec un `#` et le commentaire sur plusieurs lignes, encadré par des `"""`.

```python
# Ceci est un commentaire sur une seule ligne.

"""
Ceci est un commentaire
qui tient
sur plusieurs
lignes.
"""
```

L'instruction `print()` permet d'afficher une chaîne de caractères à l'écran. Notez la présence des guillements.

```python
print("Hello World!") # Affiche Hello World!
```

On reviendra plus en détail sur comment utiliser l'instruction `print()` pour afficher des nombres quand on parlera des variables.

Penchons nous maintenant sur la deuxième partie du code de notre _Hello World_.

```python
if __name__ == "__main__":
    hello_world()
```

Quand vous lancez un script Python, tout ce qui est en dessous de l'instruction `if __name__ == "__main__":` sera exécuté.
Ici en l'occurence, on veut appeler notre fonction `hello_world()`.

Maintenant que tout est clair (je l'espère), on va pouvoir lancer notre programme.
Visual Studio Code a un Terminal intégré pour éviter d'avoir à changer constamment de fenêtres. Vous pouvez ouvrir un Terminal en allant dans _Terminal -> Nouveau terminal_

Un Terminal se lance en bas de la fenêtre.
![Terminal](images/cd.jpg)

1. Naviguez jusqu'au dossier où se trouve votre fichier Hello World avec la commande `cd "chemin"` et appuyez sur Entrée.
   Dans mon cas, il est dans `C:\Users\hakim\Desktop\Code\Tuto_Python\Scripts\`

2. Une fois dans le bon dossier, vous pouvez lancer le programme grâce à la commande `python .\helloWorld.py`
3. Si tout se passe bien, Hello World ! s'affiche.

Bravo, vous venez de lancer votre premier programme en Python ! \o/
