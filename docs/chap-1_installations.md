## Installation de Python

Tout d'abord il vous faut la dernière version de Python, téléchargeable [ici](https://www.python.org/downloads/).
![Téléchargement de Python](images/python-dl.jpg)

Installez-la et ensuite ouvrez un Terminal.

Appuyez sur ++win+r++ en même temps, tapez `cmd` puis appuyez sur Ok.
![Lancement du Terminal sous Windows](images/cmd.jpg)

Une fois le Terminal ouvert, tapez `python --version` puis appuyez sur Entrée.
Normalement (si Python est bien installé), la commande affiche la version de Python actuellement installée (ne vous inquiétez pas si vous n'avez pas la même version que celle sur ma capture d'écran - il va sûrement y avoir une version plus récente que celle qui tourne sur ma machine à l'heure où j'écris ces lignes).

Si la commande renvoie une erreur, c'est sûrement que Python n'est pas ou mal installé donc essayez de le ré-installer.
![Affichage de la version de Python dans le Terminal](images/pythonVersion.jpg)

## Installation de Visual Studio Code

Prochaine étape, installer Visual Studio Code qui va servir à écrire notre code. Vous pouvez le télécharger [ici](https://code.visualstudio.com).
![VSCode](images/vscode-dl.jpg)
Une fois téléchargé et installé, on va installer les extensions Python qui vont nous aider à coder plus efficacement.
Pour installer une extension, il suffit d'ouvrir le panel des extensions en cliquant sur le logo avec les quatre carrés dans le menu à gauche. Puis taper le nom de l'extension que vous voulez installer, appuyer sur Entrée puis cliquer sur le bouton Installer
Ici en l'occurence, on va commencer avec le _Python Extension Pack_
![Python Extension Pack](images/python-pack.jpg)

En suivant le même procédé, vous pouvez installer _indent-rainbow_ et _Pylance_
![Indent Rainbow](images/indent-rainbow.jpg)
<br>
![Pylance](images/pylance.jpg)

Avec tout ça, vous êtes prêt‧e‧s à enchaîner sur le chapitre 2 où vous allez lancer votre premier script Python !
