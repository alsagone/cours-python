## _FizzBuzz_

### Principe

Le jeu _FizzBuzz_ se déroule de la façon suivante:

- deux personnes comptent tour à tour jusqu'à une certaine limite.
- Si le nombre est un multiple de 3, la personne doit dire _Fizz_
- Si le nombre est un multiple de 5, la personne doit dire _Buzz_
- Si le nombre est à la fois un multiple de 3 et un multiple de 5, la personne doit dire _FizzBuzz_

Par exemple, jouer à _Fizzbuzz_ jusqu'à 20 donne:
`1 2 Fizz 4 Buzz Fizz 7 8 Fizz Buzz 11 Fizz 13 14 FizzBuzz 16 17 Fizz 19 Buzz`

### Code

On va coder une fonction qui prend en paramètre un nombre `n` nous affiche le déroulé de FizzBuzz jusqu'à n (inclus).

Là où _FizzBuzz_ est intéressant, c'est qu'il y a plusieurs façons plus ou moins optimisées de tacler ce problème - au point où c'est parfois demandé aux entretiens d'embauche pour des postes de développeur‧euse.

Ici, je vais vous expliquer pas à pas le raisonnement, vous montrer les pièges à éviter et on va bien évidemment se contenter de la version simple.

Pour commencer, on sait qu'on doit aller de 1 jusqu'à `n` donc on doit utiliser une boucle `for`

```python
def fizz_buzz(n):
    debut = 1
    fin = n
    for i in range(debut, fin+1):
		# À compléter
```

Ensuite, on peut travailler sur les conditions: on doit tester si `i` est un multiple de 3 ou un multiple de 5 ou à la fois un multiple de 3 et de 5.

```python
def fizz_buzz(n):
    debut = 1
    fin = n
    for i in range(debut, fin+1):
        # Si c'est un multiple de 3, afficher "Fizz"
        if (i % 3 == 0):
            print("Fizz", end=" ")

        # Sinon si c'est un multiple de 5, afficher "Buzz"
        elif (i % 5 == 0):
            print("Buzz", end=" ")

        # Sinon si c'est un multiple de 3 et de 5, afficher "FizzBuzz"
        elif (i % 3 == 0 and i % 5 == 0):
            print("FizzBuzz", end=" ")

        # Sinon, afficher le nombre
        else:
            print(i, end=" ")

    print("")
    return
```

Si on teste notre fonction avec `n = 20`, ça nous donne:
`1 2 Fizz 4 Buzz Fizz 7 8 Fizz Buzz 11 Fizz 13 14 Fizz 16 17 Fizz 19 Buzz`

Vous remarquez que pour 15 (qui est un multiple de 3 et de 5), la fonction affiche `Fizz` au lieu de `FizzBuzz`, pourquoi ?

C'est parce que la façon dont est structurée notre `if` à l'heure actuelle fait que si un nombre est un multiple de 3, le programme ne vérifie pas les autres `if` en dessous et donc ne teste pas si le nombre est _aussi_ un multiple de 5.

Pour éviter ça, il suffit de commencer par tester si le nombre est un multiple de 3 _et_ de 5 et ensuite si c'est faux, tester les autres cas.

Ce qui donne:

```python
def fizz_buzz(n):
    debut = 1
    fin = n
    for i in range(debut, fin+1):
        # Si c'est un multiple de 3 et de 5, afficher "FizzBuzz"
        if (i % 3 == 0 and i % 5 == 0):
            print("FizzBuzz", end=" ")

        # Sinon si c'est un multiple de 3, afficher "Fizz"
        elif (i % 3 == 0):
            print("Fizz", end=" ")

        # Sinon si c'est un multiple de 5, afficher "Buzz"
        elif (i % 5 == 0):
            print("Buzz", end=" ")

        # Sinon, afficher le nombre
        else:
            print(i, end=" ")

    print("")
    return
```

En testant pour `n = 20`, ça nous donne le bon résultat `1 2 Fizz 4 Buzz Fizz 7 8 Fizz Buzz 11 Fizz 13 14 FizzBuzz 16 17 Fizz 19 Buzz`

On peut encore légèrement optimiser notre fonction: devoir taper deux fois `i % 3 == 0` et `i % 5 == 0` est assez redondant - d'autant plus que dans le cas où un nombre est seulement un multiple de 5 (ou pas du tout un multiple de 3 ou de 5):

- la fonction teste une première fois si `i % 3` et `i % 5` sont égaux tous les deux à zéro
- c'est faux donc la fonction reteste si `i % 3` vaut zéro
- c'est faux donc la fonction reteste si `i % 5` vaut zéro

C'est assez redondant et si par exemple, on veut tester _FizzBuzz_ en allant jusqu'à `1 000 000` ces tests "en double" auront un impact sur la vitesse d'exécution du programme.

Pour palier à cela, on peut utiliser deux variables `estUnMultipleDeTrois` et `estUnMultipleDeCinq` pour stocker un booléen qui représente le résultat du test de `i % 3 == 0` et `i % 5 == 0`.
La version finale de _FizzBuzz_ est donc:

```python
def fizz_buzz(n):
    debut = 1
    fin = n
    for i in range(debut, fin+1):
        estUnMultipleDeTrois = (i % 3 == 0)
        estUnMultipleDeCinq = (i % 5 == 0)

        # Si c'est un multiple de 3 et de 5, afficher "FizzBuzz"
        if estUnMultipleDeTrois and estUnMultipleDeCinq:
            print("FizzBuzz", end=" ")

        # Sinon si c'est un multiple de 3, afficher "Fizz"
        elif estUnMultipleDeTrois:
            print("Fizz", end=" ")

        # Sinon si c'est un multiple de 5, afficher "Buzz"
        elif estUnMultipleDeCinq:
            print("Buzz", end=" ")

        # Sinon, afficher le nombre
        else:
            print(i, end=" ")

    print("")

    return

if __name__ == "__main__":
    fizz_buzz(20)
    # Affiche 1 2 Fizz 4 Buzz Fizz 7 8 Fizz Buzz 11 Fizz 13 14 FizzBuzz 16 17 Fizz 19 Buzz
```

Pour finir, je me permets de vous conseiller l'excellente vidéo de [Tom Scott](https://twitter.com/tomscott) qui parle en détail de _FizzBuzz_

La vidéo est en anglais mais les sous-titres français sont disponibles :)

<iframe width="560" height="315" src="https://www.youtube.com/embed/QPZ0pIK_wsc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
## _Plus ou moins ?_

Dans cette section, on va coder le fameux jeu du _Plus ou moins ?_ où il faut deviner un nombre entre 1 et une limite.
Le programme final ressemblera à ça:
![Jeu du plus ou moins](images/plusOuMoins.png)

### Pré-requis

En plus de tout ce qu'on a appris jusqu'à présent, on a besoin d'apprendre à:

- générer un nombre aléatoire
- demander à l'utilisateur‧trice de rentrer un nombre et le récupérer

#### Générer un nombre aléatoire

En plus des fonctions de base de Python, il y a ce qu'on appelle des _librairies_ - voyez ça comme une liste de fonctions déjà toutes faites que vous pouvez facilement importer dans votre code.

Par exemple, il y a des libraires pour travailler avec des images ou même envoyer des _tweets_ sur _Twitter_.

Ici en l'occurence, la librairie pour générer des nombres aléatoires s'appelle `random` et on l'importe dans notre code simplement grâce à l'instruction `import random`

Pour générer un nombre entier aléatoire, `random` a une fonction qui s'appelle `randint` qui prend en paramètres deux nombres `a` et `b` et renvoie un nombre entier aléatoire compris entre `a` et `b` (inclus).

Par exemple, pour générer un nombre entier aléatoire compris entre 1 et 100:

```python
import random
nombre_aleatoire = random.randint(1, 100)
```

#### Demander à l'utilisateur‧trice de rentrer quelque chose

Nul besoin de librairie cette fois-ci, pour demander à l'utilisateur‧trice de rentrer quelque chose il y a l'instruction `input` qui prend en paramètre le message à afficher (par exemple: "Entrez votre prénom: ") et renvoie ce que l'utilisateur‧trice a tapé, on peut donc s'en servir pour stocker ce résultat dans une variable.

```python
prenom = input("Entrez votre prénom: ")
print(f"Bonjour {prenom} !")
```

![Fonction entrer votre prénom](images/entrerPrenom.jpg)

Dans le cas où on veut récupérer un chiffre, on peut englober l'instruction `input` à l'intérieur de l'instruction `int`.
`input` récupère une chaîne de caractères et ensuite l'instruction `int` la convertit en un entier.

```python
nombre = int(input("Entrez un nombre: "))
n = nombre + 5
print(f"{nombre} + 5 = {n}")
```

![Entrer un nombre](images/inputNombre.jpg)

Que se passe-t-il si on rentre autre chose qu'un nombre ?

Python fait bien les choses et dans ce cas, il lève une exception appelée `ValueError` et vous indique que vous vous êtes trompé.
![ValueError](images/valueError.jpg)

### Code

Pour commencer notre fonction, on a besoin:

- de définir le nombre limite que ne doit pas dépasser notre nombre aléatoire
- d'importer la librairie `random` et générer le nombre aléatoire entre 1 et cette limite
- de deux variables `minimum` et `maximum` pour indiquer à l'utilisateur‧trice les limites
- d'un booléen `trouve` qu'on initialise à `False` pour que le jeu tourne tant qu'on n'a pas trouvé le nombre
- d'une variable `nbCoups` initialisée à `0` pour compter le nombre de coups

```python
import random

def plus_ou_moins(limite):
    nombre_secret = random.randint(1, limite)
    minimum = 1
    maximum = limite
    trouve = False
    nb_coups = 0

	# À compléter

    return

if __name__ == "__main__":
    plus_ou_moins(100)
```

Maintenant, on peut commencer notre boucle `while`:

- Tant que la variable `trouve` vaut `False`:
  - Augmenter le nombre de coups de 1
  - Demander à entrer un nombre entre `minimum` et `maximum`
  - Tester si le nombre est plus petit / plus grand que le nombre à trouver et mettre à jour les variables `minimum` / `maximum` en conséquence.
  - Si le nombre n'est ni plus petit, ni plus grand que le nombre à trouver, la variable `trouve` prend la valeur `True` ce qui a pour effet que la boucle `while` s'arrête.

```python
import random


def plus_ou_moins(limite):

    nombre_secret = random.randint(1, limite)
    minimum = 1
    maximum = limite
    trouve = False
    nb_coups = 0

    while not(trouve):
        nb_coups += 1

        nombre_entre = int(input(f"Entrez un nombre compris entre {minimum} et {maximum}: "))

        if (nombre_entre < nombre_secret):
            print("Trop petit !\n")
            minimum = max(minimum, nombre_entre + 1)

        elif (nombre_entre > nombre_secret):
            print("Trop grand !\n")
            maximum = min(maximum, nombre_entre - 1)

        else:
            print(f"Bravo vous avez trouvé le nombre secret en {nbCoups} coups !")
            trouve = True

    return
```

#### Explication de la mise à jour des variables `minimum` et `maximum`

Disons qu'on commence et qu'on doit trouver un nombre compris entre 1 et 100.

On rentre 35 et le programme nous dit que c'est trop petit.

On sait donc que tous les nombres en dessous de 35 sont forcément tous également trop petits.

Donc notre nouveau minimum est 36.

Le `max(mininmum, nombre_entre + 1)` est là pour faire attention à ce que le minimum ne bouge pas si l'utilisateur entre un nombre qui est trop petit et en dehors des limites.

Disons qu'on doive trouver un nombre entre 36 et 100 et que par inadvertance, on entre 8.

On n'a pas envie que notre nouveau minimum soit égal à 9 parce que ce serait trompeur (vrai mais trompeur quand même :D).

On utilise donc l'instruction `max` qui prend en paramètre deux nombres et renvoie le plus grand des deux.

Dans notre cas, on compare le minimum actuel (36) et le potentiel nouveau minimum (9 = 8 + 1).

36 est plus grand que 9 donc le minimum reste 36.

Idem pour la variable `maximum`.

On doit trouver un nombre entre 36 et 100, on entre 85 et c'est trop grand.

On sait d'office que tous les nombres au-dessus de 85 sont eux aussi trop grands donc le nouveau maximum est 84.

L'instruction `min(maximum, nombre_entre - 1)` est là pour faire attention à ce que le maximum ne bouge pas si l'utilisateur entre un nombre qui est trop grand et en dehors des limites.

Disons qu'on doive trouver un nombre entre 36 et 85 et que par inadvertance, on entre 90.

On doit comparer le minimum entre le maximum actuel et le potentiel nouveau maximum.

On utilise donc l'instruction `min` qui prend en paramètre deux nombres et renvoie le plus petit des deux.

Dans notre cas, on compare le maximum actuel (85) et le potentiel nouveau maximum (89 = 90 - 1).

85 est plus petit que 89 donc le maximum reste à 85.
