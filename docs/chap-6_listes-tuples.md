## Les listes

Les listes sont un moyen de travailler avec beaucoup de données tout en manipulant qu'une seule variable.

Disons que vous êtes prof et que vous voulez travailler avec les notes de vos élèves. Si vous avez une petite classe de 5 élèves, vous pouvez bien entendu déclarer 5 variables:

```python
note_bob = 15
note_alice = 16.5
note_michel = 12
note_laure = 15
note_arnaud = 17
```

Mais si vous avez plusieurs dizaines d'élèves, ça devient vite ingérable. C'est là qu'interviennent les listes !

#### Déclarer une liste

Pour déclarer une liste, on utilise des crochets `[]` et on sépare les éléments avec une virgule.

```python
notes = [15, 16.5, 12, 15, 17, 18.5, 13.5, 19, 16, 16, 16.5, 15.5, 10, 14.5, 19.5, 14, 14.5, 17.5, 11, 14.5]

# Ou si on veut déclarer une liste vide
ma_liste = []
```

On peut bien évidemment travailler avec n'importe quel type de données: des listes de chaînes de caractères, de variables ou même de booléens

```python
liste_noms = ["Bob", "Alice", "Fred"]

temperature_metz = 15
temperature_lyon = 20
temperature_brest = 14
liste_temperatures = [temperature_metz, temperature_lyon, temperature_brest]
# C'est un mauvais exemple parce que dans ce cas, on préférera
# directement déclarer liste_temperatures = [15, 20, 14]
# mais c'est juste pour montrer qu'on peut travailler avec des variables

liste_booleens = [True, False, True]
```

Et même si ce n'est **absolument pas recommandé**, il n'est pas nécessaire que tous les éléments d'une liste soient du même type.

```python
liste_soupe = ["Bob", 25, True]
```

N'ayez pas peur de vous retrouver avec de longues listes: la longueur limite est si grande qu'avoir une liste de 100 000 000 éléments est parfaitement possible.
Si par curiosité, vous voulez savoir la limite exacte que votre PC peut supporter, elle est stockée dans la variable `sys.maxsize` de la librairie `sys`.

```python
import sys
print(f"La longueur maximale de liste vaut {sys.maxsize}")
```

### Accéder à un élément d'une liste

On peut directement récupérer le 1er, 2ème, 3ème etc... élément d'une liste grâce à la notation `liste[i]` mais attention **les indices commencent à 0**.
Ce qui veut dire que le premier élément est `liste[0]`, le deuxième est `liste[1]` etc...

De ce fait, le dernier élément d'une liste a l'indice `longueur de la liste - 1`.

### Longueur d'une liste

Pour récupérer la longueur d'une liste, on peut utiliser la méthode `len()`.

```python
liste = [3, 5, 7, 6, 40, 27, 35]
nb_elements = len(liste)
print(f"La liste contient {nb_elements} éléments")
# La liste contient 7 éléments
```

### Trier une liste 

Note: pour trier une liste, il faut bien entendu que tous les éléments que contient la liste soient du même type.

#### Ordre croissant ou décroissant

Python a une méthode toute faite pour trier une liste, on l'appelle en faisant `liste.sort()`.

```python
liste = [9, 2, 5, 4]
liste.sort()
print(liste) # liste = [2, 4, 5, 9]
```

Par défaut, `sort()` trie par ordre croissant (du plus petit au plus grand) mais on peut préciser que l'ordre que l'on veut grâce à l'option `reverse` suivie d'un booléen (`True` ou `False`).

```python
liste = [9, 2, 5, 4]

#Tri par ordre croissant
liste.sort(reverse = False)  # C'est l'équivalent de liste.sort()
print(liste) # liste = [2, 4, 5, 9]

#Tri par ordre décroissant
liste.sort(reverse = True) 
print(liste) # liste = [9, 5, 4, 2]
```

#### Par une fonction de comparaison 

Il est aussi possible de trier une liste selon un critère défini

### Découper une liste

On peut récupérer un morceau d'une liste avec la notation `liste[debut:fin]`. Par exemple, pour récupérer tous les éléments à partir du 2ème élément et jusqu'au 5ème élément (inclus), on peut écrire `liste[1:5]`

Ça nécessite un peu de gymnastique mentale à cause du fait que les indices démarrent à 0 mais si vous voulez avoir un moyen simple de vous rappeler comment passer de la notation _du i-ème élément au j-ème élément_ à la notation Python: prendre un bout de liste qui du i-ème élément au j-ème élément est noté `liste[i-1:j]`

```python
liste = [3, 5, 7, 6, 40, 27, 35]
# Morceau du 3ème au 6ème élément inclus
morceau = notes[2:6]
print(morceau)

# Affiche [7, 6, 40, 27]
```

#### Abréviation

Python a aussi une notation "abrégée" quand il s'agit de découper une liste.

- `liste[:3]` veut dire qu'on prend tous les éléments du début jusqu'à l'indice 3. C'est l'équivalent de `liste[0:3]`.
- `liste[3:]` veut dire qu'on prend tous les éléments à partir de l'indice 3 et jusqu'à la fin. C'est l'équivalent de `liste[3:len(liste)-1]`.

#### Renverser une liste 

Avec cette notation, on peut aussi renverser une liste.

```python
liste = [6, 5, 4, 3, 2]
liste_renversee = liste[::-1]
print(liste_renversee) # [2, 3, 4, 5, 6]
```

PS: Ne vous inquiétez pas si vous ne la retenez pas toutes les notations bizarres - surtout tout ce qui est découpage de liste - ça fait un peu plus de 3 ans que je me suis lancé dans l'apprentissage du Python et il y a toujours des moments où je ne me souviens pas des bases et même des développeur‧euse‧s dont c'est le métier doivent parfois re-regarder les bases donc ne vous en faites pas ! 💜

### Fusionner deux listes

On peut fusionner/concaténer deux listes avec l'opérateur `+`

```python
liste_1 = [1, 2, 3]
liste_2 = [27, 38, 52]
fusion = liste_1 + liste_2
print(fusion) # fusion = [1, 2, 3, 27, 38, 52]
```

### Parcourir une liste

#### Avec un index

On a vu que le début d'une liste commence à 0 et qu'une liste finit à l'indice `longueur de la liste - 1`.
On peut donc écrire une boucle `for` qui part de 0 et qui s'arrête à `longueur de la liste - 1`.

Par exemple, pour afficher tous les éléments d'une liste:

```python
liste = [3, 5, 7, 6, 40, 27, 35]
debut = 0
fin = len(liste) - 1
for i in range(debut, fin+1):
    print(liste[i], end=" ")

print("")
```

#### Version condensée

Python permet aussi de parcourir l'intégralité d'une liste sans se soucier des indices.

```python
liste = [3, 5, 7, 6, 40, 27, 35]

for i in liste:
    print(i, end=" ")

print("")
```

On utilise la première version si dans notre programme, on doit se soucier de l'indice d'un élément. Par exemple, si vous voulez récupérer tous les éléments d'une liste qui ont un indice pair, on ne peut pas utiliser la version condensée.

### Récupérer l'indice d'un élément dans une liste

Avec la méthode `index` et l'élément dont on souhaite récupérer l'indice

```python
liste = [2, 5, 8]

indice_cinq = liste.index(5)

print(f"L'indice de 5 dans la liste est {indice_cinq}");

# L'indice de 5 dans la liste est 1.
```

Si l'élément qu'on cherche n'est pas dans la liste, Python lève une exception `ValueError`.
![ValueError](images/liste_indice.jpg)

### Vérifier si un élément est dans une liste

On fait ça grâce au test `element in liste`

```python
liste = [2, 5, 8]

if (1 in liste):
    print("1 est dans la liste")
    
else: 
    print("1 n'est pas dans la liste")
```

Si on reprend l'exemple de la section précédente, faire ce test vous évite d'avoir une `ValueError` si vous cherchez à récupérer l'indice d'un élément dans une liste

```python
liste = [2, 5, 8]

if (1 in liste):
    indice = liste.index(1)
    
else:
    print("1 n'est pas dans la liste")
```

Ici, on vérifie d'abord si 1 est dans la liste et si oui, on récupère son indice.
On a vu tout à l'heure sur cet exemple que l'instruction `liste.index(1)` renvoyait une erreur. Mais là, on la contourne en vérifiant d'abord si 1 est dans la liste, vu qu'il n'y est pas, le programme affiche simplement `1 n'est pas dans la liste` sans renvoyer d'erreur.

### Insérer un élément d'une liste

#### Insérer en fin de liste

On utilise la méthode `append` puis la valeur qu'on veut ajouter entre parenthèses.

```python
liste = [2, 3, 5]
liste.append(8)
print(liste) # liste = [2, 3, 5, 8]
```

#### Insérer à un endroit précis

On utilise la méthode `insert` avec entre parenthèses l'indice et la valeur à insérer.

```python
liste = [2, 3, 5]

# Insérer 8 à l'indice 1
liste.insert(8, 1)

print(liste) # liste = [2, 8, 3, 5]

# Avec cette méthode, on peut insérer en début de liste en faisant un liste.insert(element, 0)
liste.insert(27, 0)

print(liste) # liste = [27, 2, 8, 3, 5]
```

### Supprimer un élément d'une liste

#### Avec la méthode _remove_

Syntaxe: `liste.remove(element)`

```python
liste = [2, 3, 5]
liste.remove(3)
print(liste) # liste = [2, 5]
```

Là encore, si on veut supprimer un élément qui n'existe pas dans la liste, Python lève une exception `ValueError`.

#### En découpant la liste

Disons qu'on a envie de supprimer le 3ème élément d'une liste (l'élément d'indice 2)
On peut découper la liste en deux parties:

- ce qui est à gauche du 3ème élément -> les éléments d'indice 0 et 1
- ce qui est à droite du 3ème élément -> tous les éléments à partir de l'indice 3 jusqu'à la fin

Puis les fusionner.

```python
liste = [2, 3, 5, 7, 9, 12]

# On veut supprimer le 3ème élément - celui d'indice 2
partie_gauche = liste[:2]
print(partie_gauche)  # partie_gauche = [2, 3]

partie_droite = liste[3:]
print(partie_droite)  # partie_droite = [7, 9, 12]

nouvelle_liste = partie_gauche + partie_droite
print(nouvelle_liste)
# nouvelle_liste = [2, 3, 7, 9, 12]
```

Et si jamais il vous faut une fonction toute faite:

```python
# Prend en paramètre une liste et l'indice de l'élément à supprimer
# Retourne une nouvelle liste

def supprimerElementListe(liste, indice):
    return liste[:indice-1] + liste[indice:]

liste = [2, 3, 5, 7, 9, 12]
nouvelle_liste = supprimerElementListe(liste, 3)
print(nouvelle_liste) # [2, 3, 7, 9, 12]
```

## Les tuples

Les tuples sont comme des listes mais ils ne peuvent que contenir que deux éléments. Ils sont utilisés dans les cas où on ne doit que stocker deux éléménets, par exemple la marque d'une voiture et son prix, le nom d'un élève et sa moyenne, les coordonnées d'un point etc...
On déclare un tuple avec des parenthèses, suivies des deux valeurs séparées par une virgule.

```python
volvo = ("Volvo", 2500)
alfred = ("Alfred", 12.7)
point_a = (5, 5)
```

Les tuples sont **immuables**, ce qui veut dire qu'on ne peut pas modifier leur valeur après les avoir déclarés.

Pour accéder à une des deux valeurs d'un tuple, c'est comme les listes sauf qu'ici on est limités aux indices 0 et 1.

```python
volvo = ("Volvo", 2500)
marque = volvo[0] # "Volvo"
prix = volvo[1] # 2500
```

Vous pouvez bien entendu créer une liste du tuples.

```python
liste_tuples = [("Volvo", 2500), ("Audi", 3000), ("Peugeot", 2650)]

for t in liste_tuples:
    nom = t[0]
    prix = t[1]
    print(f"{nom}: {prix}€")
    
 """
 	Affiche 
 	Volvo: 2500€
	Audi: 3000€
	Peugeot: 2650€
 """

```

