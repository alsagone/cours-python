# Cours d'introduction au Python

Bonjour ! o/<br>
Vu que je me débrouille plutôt pas mal en Python et que mes ami‧e‧s me demandent souvent comment j'ai fait pour apprendre, je me suis dit que j'allais écrire une petite série de cours avec pour objectif qu'ils soient compréhensibles par une personne qui n'a jamais touché un seul langage de programmation.

À l'heure où j'écris cette introduction, je n'ai pas encore planché sur une structure définitive pour mes chapitres mais je pense inclure un ou deux exercices simples à faire puis mettre une correction détaillée en dessous.

Si jamais vous avez le moindre problème ou la moindre question, vous pouvez m'envoyer un message sur [Twitter](https://twitter.com/alsagone), j'y réponderai avec plaisir !

PS: si ça vous plaît et si vous voulez me donner un peu de sous, j'ai un [Ko-fi](https://ko-fi.com/alsagone) ! :)

<img src="images/kofi_ours_bonjour_gay.png" style="margin: 0 auto; display: block" alt="PayPal Ours Bonjour" />
