"""
Prend en paramètre un nombre n et renvoie:
	"Fizz" si le nombre est un multiple de 3 (et pas un multiple de 5)
	"Buzz" si le nombre est un multiple de 5 (et pas un multiple de 3)
	"FizzBuzz" si le nombre est à la fois un multiple de 3 et de 5
	le nombre sous forme de chaîne s'il n'est pas un multiple de 3 ni de 5
"""


def get_fizz_buzz(n):
    # On part avec un resultat vide
    resultat = ""

    # On rajoute Fizz au resultat si le nombre est un multiple de 3
    if (n % 3 == 0):
        resultat += "Fizz"

    # On rajoute Buzz au resultat si le nombre est un multiple de 5
    # Du coup, si le nombre à la fois un multiple de 3 et de 5, on a FizzBuzz
    if (n % 5 == 0):
        resultat += "Buzz"

    # Si le resultat est vide à ce stade du code, ça veut dire que le nombre n'est ni un multiple de 3,
    # ni un multiple de 5 donc le résultat est simplement le nombre converti en string
    if (len(resultat) == 0):
        resultat = str(n)

    return resultat


def fizz_buzz_v2(limite):
    debut = 1
    fin = limite + 1
    for i in range(debut, fin):
        print(get_fizz_buzz(i), end=" ")  # On sépare le tout par des espaces

    print()  # Retour à la ligne une fois fini
    return


if __name__ == "__main__":
    fizz_buzz_v2(20)
