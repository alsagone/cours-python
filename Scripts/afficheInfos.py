def afficheInfos(nom, age, salaire):
    print(f"Je m'appelle {nom}, j'ai {age} ans et j'ai {salaire} euros.")
    return


if __name__ == "__main__":
    afficheInfos("Bob", 26, 1034.73)
    afficheInfos("Alice", 35, 2048.10)

"""
  Affiche
  Je m'appelle Bob, j'ai 26 ans et j'ai un salaire de 1034.73 euros.
  Je m'appelle Alice, j'ai 35 ans et j'ai un salaire de 2048.10 euros.
"""
