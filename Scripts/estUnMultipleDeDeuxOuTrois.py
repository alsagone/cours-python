def estUnMultipleDeDeuxOuTrois(n):
    if (n % 2 == 0):
        print(f"{n} est un multiple de 2.")

    elif(n % 3 == 0):
        print(f"{n} est un multiple de 3.")

    else:
        print(f"{n} n'est ni un multiple de 2, ni un multiple de 3.")

    return


if __name__ == "__main__":
    estUnMultipleDeDeuxOuTrois(10)
    estUnMultipleDeDeuxOuTrois(33)
    estUnMultipleDeDeuxOuTrois(37)

"""
    Affiche
    10 est un multiple de 2.
    33 est un multiple de 3.
    37 n'est ni un multiple de 2, ni un multiple de 3.
"""
