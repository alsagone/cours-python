# Prend en paramètre deux nombres (a et b) et affiche leur somme
def somme(a, b):
    s = a + b
    print(f"{a} + {b} = {s}")


if __name__ == "__main__":
    somme(10, 8)
    somme(2, 50)
    somme(5, 3)

"""
    Affiche
    10 + 8 = 18
    2 + 50 = 52
    5 + 3 = 8
"""
