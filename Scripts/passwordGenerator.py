import enum
import sys
import random


class Caractere(enum.Enum):
    MAJUSCULE = 0
    MINUSCULE = 1
    CHIFFRE = 2
    LETTRE = 3
    SYMBOLE = 4
    NON_DEFINI = 5


minuscules = "abcdefghijklmnopqrstuvwxyz"
majuscules = minuscules.upper()
chiffres = "0123456789"
symboles = "!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~"

symboles_init = "!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~"


def get_type(caractere):
    t = Caractere.NON_DEFINI
    global symboles_init

    if (caractere.isdigit()):
        t = Caractere.CHIFFRE

    elif (caractere.isalpha()):
        t = Caractere.MAJUSCULE if caractere.isupper() else Caractere.MINUSCULE

    elif (caractere in symboles_init):
        t = Caractere.SYMBOLE

    else:
        print(f"Erreur caractère inconnu: {caractere}")
        sys.exit(1)

    return t


def types_disponibles(type_a_eviter, alphanumerique):
    liste_types = []
    global minuscules, majuscules, chiffres, symboles

    if (type_a_eviter != Caractere.MAJUSCULE and len(majuscules) > 0):
        liste_types.append(Caractere.MAJUSCULE)

    if (type_a_eviter != Caractere.MINUSCULE and len(minuscules) > 0):
        liste_types.append(Caractere.MINUSCULE)

    if (type_a_eviter != Caractere.CHIFFRE and len(chiffres) > 0):
        liste_types.append(Caractere.CHIFFRE)

    if (not(alphanumerique) and type_a_eviter != Caractere.SYMBOLE and len(symboles) > 0):
        liste_types.append(Caractere.SYMBOLE)

    return liste_types


def choisir_indice_aleatoire(liste_ou_string):
    return random.randint(0, len(liste_ou_string)-1)


def type_aleatoire(type_a_eviter, alphanumerique):
    liste_types = types_disponibles(type_a_eviter, alphanumerique)
    indice_aleatoire = choisir_indice_aleatoire(liste_types)
    return liste_types[indice_aleatoire]


def supprimer_caractere(string, indice):
    return string[:indice] + string[indice+1:]


def caractere_aleatoire(type_caractere, caracteres_uniques):
    # On utilise le mot clé global pour dire à Python qu'on
    # fait référence à nos variables globales
    global minuscules, majuscules, chiffres, symboles

    c = None

    # On vérifie avec quel type de caractère on doit travailler
    # On choisit un indice aléatoire et on récupère le caractère à cet indice

    # Si on a choisi de n'avoir que des caractères uniques dans notre mot de passe,
    # on supprime le caractère dans la string dont il provient
    if (type_caractere == Caractere.MAJUSCULE):
        indice_aleatoire = choisir_indice_aleatoire(majuscules)
        c = majuscules[indice_aleatoire]

        if (caracteres_uniques):
            majuscules = supprimer_caractere(majuscules, indice_aleatoire)

    elif (type_caractere == Caractere.MINUSCULE):
        indice_aleatoire = choisir_indice_aleatoire(minuscules)
        c = minuscules[indice_aleatoire]

        if (caracteres_uniques):
            minuscules = supprimer_caractere(minuscules, indice_aleatoire)

    elif (type_caractere == Caractere.CHIFFRE):
        indice_aleatoire = choisir_indice_aleatoire(chiffres)
        c = chiffres[indice_aleatoire]

        if (caracteres_uniques):
            chiffres = supprimer_caractere(chiffres, indice_aleatoire)

    elif (type_caractere == Caractere.SYMBOLE):
        indice_aleatoire = choisir_indice_aleatoire(symboles)
        c = symboles[indice_aleatoire]

        if (caracteres_uniques):
            symboles = supprimer_caractere(symboles, indice_aleatoire)

    return c


def construire_mot_de_passe(longueur, caracteres_uniques, alphanumerique):
    mot_de_passe = ""
    type_a_eviter = Caractere.NON_DEFINI

    for i in range(1, longueur):
        type_caractere = type_aleatoire(type_a_eviter, alphanumerique)
        caractere = caractere_aleatoire(
            type_caractere, caracteres_uniques)
        mot_de_passe += caractere
        type_a_eviter = get_type(caractere)
        print(mot_de_passe)

    return mot_de_passe


if __name__ == "__main__":
    p = construire_mot_de_passe(10, True, False)
