import random


def plusOuMoins(limite):

    nombreSecret = random.randint(1, limite)
    minimum = 1
    maximum = limite
    trouve = False
    nbCoups = 0

    while not(trouve):
        nbCoups += 1

        nombreEntre = int(
            input(f"Entrez un nombre compris entre {minimum} et {maximum}: "))

        if (nombreEntre < nombreSecret):
            print("Trop petit !\n")
            minimum = max(minimum, nombreEntre + 1)

        elif (nombreEntre > nombreSecret):
            print("Trop grand !\n")
            maximum = min(maximum, nombreEntre - 1)

        else:
            print(
                f"Bravo vous avez trouvé le nombre secret en {nbCoups} coups !")
            trouve = True

    return


if __name__ == "__main__":
    plusOuMoins(100)
