def fizzbuzz(n):
    debut = 1
    fin = n
    for i in range(debut, fin+1):
        estUnMultipleDeTrois = (i % 3 == 0)
        estUnMultipleDeCinq = (i % 5 == 0)

        # Si c'est un multiple de 3 et de 5, afficher "FizzBuzz"
        if estUnMultipleDeTrois and estUnMultipleDeCinq:
            print("FizzBuzz", end=" ")

        # Si c'est un multiple de 3, afficher "Fizz"
        elif estUnMultipleDeTrois:
            print("Fizz", end=" ")

        # Si c'est un multiple de 5, afficher "Buzz"
        elif estUnMultipleDeCinq:
            print("Buzz", end=" ")

        # Sinon, afficher le nombre
        else:
            print(i, end=" ")

    print("")

    return


if __name__ == "__main__":
    fizzbuzz(20)
